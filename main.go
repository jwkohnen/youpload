package main

import (
	"context"
	"fmt"
	"io"
	"os"

	"gitlab.com/jwkohnen/yup/config"
	"gitlab.com/jwkohnen/yup/pkg"
)

func main() {
	cfg, err := config.ConfigureFromOSArgs(os.Args)
	if err != nil {
		_, _ = fmt.Fprintf(os.Stderr, "error: %v\n", err)
		os.Exit(2)
	}

	fmt.Printf("%#v\n", cfg)

	fmt.Println(pkg.OpenStream(cfg.Sources))

	s, err := pkg.New(context.TODO(), cfg.Sources)
	if err != nil {
		panic(err)
	}

	r := s.Reader()

	if err := s.Start(); err != nil {
		panic(err)
	}

	n, err := io.Copy(os.Stdout, r)
	if err != nil {
		panic(err)
	}

	if err := s.Close(); err != nil {
		panic(err)
	}

	_, _ = fmt.Fprintf(os.Stderr, "copied %d bytes\n", n)
}
