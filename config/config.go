package config

import (
	"errors"
	"flag"
	"fmt"
	"path/filepath"
)

type Config struct {
	Sources []string
	Verbose bool
}

func defaults() Config {
	return Config{Verbose: false}
}

func ConfigureFromOSArgs(args []string) (*Config, error) {
	cfg := defaults()

	fs := flag.NewFlagSet(args[0], flag.ExitOnError)
	fs.BoolVar(&cfg.Verbose, "verbose", cfg.Verbose, "verbosity")

	err := fs.Parse(args[1:])
	if err != nil {
		return nil, err
	}

	src := fs.Args()
	if len(src) == 0 {
		return nil, errors.New("no source files given")
	}

	if cfg.Sources, err = abs(src); err != nil {
		return nil, err
	}

	return &cfg, nil
}

func abs(files []string) ([]string, error) {
	abs := make([]string, len(files))

	for i, f := range files {
		var err error

		if abs[i], err = filepath.Abs(filepath.Clean(f)); err != nil {
			return nil, fmt.Errorf("could not make path %q absolute: %w", f, err)
		}
	}

	return abs, nil
}
