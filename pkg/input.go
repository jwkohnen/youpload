package pkg

import (
	"bufio"
	"context"
	"fmt"
	"io"
	"io/fs"
	"os"
	"os/exec"
	"strings"
)

func OpenStream(src []string) error {
	if err := checkRegulars(src); err != nil {
		return err
	}

	return nil
}

type Stream struct {
	ffmpeg  *exec.Cmd
	r       io.Reader
	tmpFile string
}

func New(ctx context.Context, src []string) (stream *Stream, err error) {
	sourceListFileName := "concat:" + strings.Join(src, "|")

	args := []string{"-i", sourceListFileName, "-c", "copy", "-bsf:a", "aac_adtstoasc", "-f", "mpegts", "pipe:"}

	ffmpeg := exec.CommandContext(ctx, "ffmpeg", args...)

	ffmpeg.Stderr = os.Stderr

	r, err := ffmpeg.StdoutPipe()
	if err != nil {
		panic(err)
	}

	return &Stream{ffmpeg: ffmpeg, r: r, tmpFile: sourceListFileName}, nil
}

func _(src []string) (fileName string, err error) {
	tmpFile, err := os.CreateTemp("", "yup-inputs")
	if err != nil {
		return "", fmt.Errorf("could not create temp file: %w", err)
	}

	defer func() {
		if errClose := tmpFile.Close(); err == nil {
			err = errClose
		}
	}()

	fileName = tmpFile.Name()

	w := bufio.NewWriter(tmpFile)
	defer func() {
		if errFlush := w.Flush(); err == nil {
			err = errFlush
		}
	}()

	var nn int64

	for _, inputFileName := range src {
		n, err := fmt.Fprintf(w, "file '%s'\n", inputFileName)
		nn += int64(n)
		if err != nil {
			return "", fmt.Errorf("could not write to temp file: %w", err)
		}
	}

	_, _ = fmt.Fprintf(os.Stderr, "written %d bytes to tempfile %q\n", nn, fileName)

	return fileName, nil
}

func (s *Stream) Start() error {
	if err := s.ffmpeg.Start(); err != nil {
		return fmt.Errorf("could not start ffmpeg: %w", err)
	}

	return nil
}

func (s *Stream) Reader() io.Reader { return s.r }

func (s *Stream) Close() (err error) {
	/*
		defer func() {

			if errRemove := os.Remove(s.tmpFile); err == nil {
				err = fmt.Errorf("could not remove tempfile %q: %w", s.tmpFile, errRemove)
			}
		}()

	*/

	if err := s.ffmpeg.Wait(); err != nil {
		return fmt.Errorf("error while running %v: %w", s.ffmpeg.Args, err)
	}

	return nil
}

func checkRegulars(src []string) error {
	for _, s := range src {
		stat, err := os.Stat(s)
		if err != nil {
			return fmt.Errorf("could not open file %q: %w", s, err)
		}

		if stat.Mode()&fs.ModeType != 0 {
			return fmt.Errorf("file is not a regular file: %q", s)
		}
	}

	return nil
}
